int num = 1000;
float[] r = new float[num];

float theta;
float theta_vel;
float theta_acc;
color[] clr = new color[num];
void setup(){
  size(500, 500);
  for(int i =0; i< num; i++){
r[i] = height * 0.35;
clr[i] = color(random(255), 0, random(255));
//clr[i] = color(sin(i)*255, 0, cos(i)*255);
  }
theta = 0;
background(0);

}

void draw(){
fill(0, 3);
float counter = frameCount;
rect(0, 0, width, height);
theta = counter;
  for(int i =0; i< num; i++){
float mult = 0.01*i;
r[i]  += cos(frameCount*mult)*2;
  }
ellipseMode(CENTER);
noStroke();
translate( width/2, height/2);

  for(int i =0; i< num; i++){
    
float x = r[i] * cos(theta);
float y = r[i]* sin(theta);

fill(clr[i]);
ellipse(x,y, 5, 5);
  }


}
